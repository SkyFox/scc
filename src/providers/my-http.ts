import { Injectable } from '@angular/core';
import { Http,Response,Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';


@Injectable()
export class MyHttp {
  public baseUrl ="http://112.74.169.23:6060/sccomp/app/";
  //public baseUrl ="http://192.168.8.103:8082/sccomp/app/";
  //public baseUrl ="http://biggsxu.oicp.net/sccomp/app/";
  constructor(public http: Http) {
  }

  get(url:string):Observable<any>{
    let allUrl = this.baseUrl + url ;
    return this.http.get(allUrl)
               .map(this.extractData)
               .catch(this.handleError);
  };

  post(url:string, data:Object,head?:Object){
    let headers = new Headers(head || { 'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let allUrl = this.baseUrl + url ;
    return this.http.post(allUrl,data, options)
               .map(this.extractData)
               .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    return Observable.throw(errMsg);
  }
}
