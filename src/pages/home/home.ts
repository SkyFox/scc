import { Component, ErrorHandler } from '@angular/core';
import { NavController, NavParams, ActionSheetController, AlertController,Platform,ModalController,Events} from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { ScListPage } from './../sc-list/sc-list';
import { ViewStationPage } from './../view-station/view-station';
import {SearchAddressPage} from './../search-address/search-address';
import { Geolocation } from 'ionic-native';
import {SearchCityPage} from './../search-city/search-city';
import {ChargingStatusPage} from './../charging-status/charging-status';
import { MyTips } from './../../providers/my-tips';
import { MyGlobals } from './../../providers/my-globals';
import { MyStoarge } from './../../providers/my-stoarge';
import { AppVersion } from 'ionic-native';

//md5 加密
import { Md5 } from "ts-md5/dist/md5";

/*
  Generated class for the Home page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public map: any;
  public toast: any;
  public userInfo:any;
  public personalMarker:any;
  public winHeight:any="600px";
  public userCity:any;
  public currCity:any;
  public stations = [];
  public myPosition = [];
  public isCharging = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public myHttp: MyHttp,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public alertCtrl: AlertController,
    public plt: Platform,
    public events: Events,
    public myTips:MyTips,
    public myGlobals:MyGlobals,
    public myStoarge:MyStoarge) {
        this.generateMap();
  }

  ionViewDidLoad() {
    this.checkAppVersion();
    this.userInfo = this.myStoarge.getObject("user_info");
    this.showCityInfo();
    this.initEvents();
  }

  initEvents(){
      this.events.subscribe('address:selected', (item) => {
            //选择好的地点
            this.map.clearMap();
            this.map.setCenter(new AMap.LngLat(item.location.lng, item.location.lat));
            new AMap.Marker({
                map: this.map,
                icon: './assets/img/mark_b1.png',
                position: [item.location.lng,item.location.lat],
                offset: new AMap.Pixel(-12, -36)
            });
      });

      this.events.subscribe('city:selected', (item) => {
          this.userCity = item.zoneCity.replace("市",""); 
          this.map.setCity(item.zoneCity);
          this.getStations(item.zoneCityId,this.userInfo&&this.userInfo.mobile||'');
       });

       this.events.subscribe('setRootPage', () => {
          console.log("---------------");
            this.generateMap();
       });

  }



  generateMap(){
      AMapUI.loadUI(['control/BasicControl'], (BasicControl)=>{
      this.map = new AMap.Map('container', {
        resizeEnable: true,
        lang: 'zh_cn',
        zoom: 15
      });
      //缩放控件
      this.map.addControl(new BasicControl.Zoom({
          position: 'lb', //left top，左上角
          showZoomNum: false, //显示zoom值
          style : {left: "20px"}

      }));

      //实时交通控件
      this.map.addControl(new BasicControl.Traffic({
          position: 'rt'//left bottom, 左下角
      }));

      if(this.plt.is('cordova')){
        this.geolocationFromNative();
      }else{
        this.geolocationFromBrowser();
      }
    });
  }

  ionViewDidEnter(){
    if(this.userInfo){
     this.checkCharingStatus();
    }
  }

  ionViewWillEnter(){
    this.winHeight = (this.plt.height()-105)+"px";
  }

  geolocationFromBrowser(){
      let myPresent= this.myTips.presentLoading("ios-small","正在定位中...");
      this.map.plugin('AMap.Geolocation', () => {
        var geolocation = new AMap.Geolocation({
              enableHighAccuracy: true,//是否使用高精度定位，默认:true
              maximumAge: 0,           //定位结果缓存0毫秒，默认：0
              convert: true,           //自动偏移坐标，偏移后的坐标为高德坐标，默认：true
              showButton: true,        //显示定位按钮，默认：true
              buttonPosition: 'LB',    //定位按钮停靠位置，默认：'LB'，左下角
              buttonOffset: new AMap.Pixel(10, 20),//定位按钮与设置的停靠位置的偏移量，默认：Pixel(10, 20)
              showMarker: true,        //定位成功后在定位到的位置显示点标记，默认：true
              showCircle: true,        //定位成功后用圆圈表示定位精度范围，默认：true
              panToLocation: true,
              useNative: true
        });
        this.map.addControl(geolocation);
        geolocation.getCurrentPosition();
        AMap.event.addListener(geolocation, 'complete', (data)=> { 
          this.myGlobals.setMyPoistion(data.position.lng, data.position.lat);
          this.getStations('027',this.userInfo&&this.userInfo.mobile||'');
          myPresent.dismiss();
          console.log(data) 
        });//返回定位信息
        AMap.event.addListener(geolocation, 'error', function (data) {
        });
      })
  }

  getMyLocation(event:Event){
        event.stopPropagation();
        Geolocation.getCurrentPosition().then((resp) => {
            this.map.setCenter(new AMap.LngLat(resp.coords.longitude, resp.coords.latitude));
            let lnglat = new AMap.LngLat(resp.coords.longitude, resp.coords.latitude);
            this.personalMarker.setPosition(lnglat);
            this.myPosition = [resp.coords.longitude, resp.coords.latitude];
            this.myGlobals.setMyPoistion(resp.coords.longitude, resp.coords.latitude);
          
        }).catch((error) => {
          let alert = this.alertCtrl.create({
                title: '定位失败',
                subTitle: 'GPS信号弱，当前定位失败',
                buttons: ['确定']
              });
              alert.present();
        });
  }

  geolocationFromNative(){
      let myPresent= this.myTips.presentLoading("ios-small","正在定位中...");
      Geolocation.getCurrentPosition().then((resp) => {
          this.map.setCenter(new AMap.LngLat(resp.coords.longitude, resp.coords.latitude));
          this.personalMarker=new AMap.Marker({
              map: this.map,
              icon: './assets/img/geo.png',
              position: [resp.coords.longitude,resp.coords.latitude],
              offset: new AMap.Pixel(-12, -36)
           });
           this.myPosition = [resp.coords.longitude,resp.coords.latitude];
           this.myGlobals.setMyPoistion(resp.coords.longitude, resp.coords.latitude);
           this.userCity = this.currCity;
           this.getStations('027',this.userInfo&&this.userInfo.mobile||'');
           myPresent.dismiss();
        
      }).catch((error) => {
        myPresent.dismiss();
        let alert = this.alertCtrl.create({
              title: '定位失败',
              subTitle: 'GPS信号弱，当前定位失败',
              buttons: ['确定']
            });
            alert.present();
      });

      let watch = Geolocation.watchPosition();
      watch.subscribe((pos) => {
          let lnglat = new AMap.LngLat(pos.coords.longitude, pos.coords.latitude);
          this.personalMarker.setPosition(lnglat);
          this.myPosition = [pos.coords.longitude,pos.coords.latitude];
          this.myGlobals.setMyPoistion(pos.coords.longitude,pos.coords.latitude);
      });
  }

  generateMarkers(){
      this.stations.forEach((station) => {
        var mapMarker = new AMap.Marker({
            map: this.map,
            icon: './assets/img/mark_b222.png',
            position: [station.longitude, station.latitude],
            offset: new AMap.Pixel(-12, -36),
            extData : {
              'station' : station,
              'myPosition' : this.myPosition
            }
        });
        mapMarker.on('click', (e) => {
          console.log(e);
          let modal = this.modalCtrl.create(ViewStationPage,e.target.G.extData);
          modal.present();
        });
      });
  }

  goToScList() {
    this.navCtrl.push(ScListPage);
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: '高德地图',
          handler: () => {
            console.log('Archive clicked');
          }
        },
        {
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  onInput(){
    this.navCtrl.push(SearchAddressPage);
  }

  //获取用户所在城市信息
  showCityInfo() {
        //实例化城市查询类
        var citysearch = new AMap.CitySearch();
        //自动获取用户IP，返回当前城市
        citysearch.getLocalCity((status, result)=>{
            if (status === 'complete' && result.info === 'OK') {
                if (result && result.city && result.bounds) {
                    var cityinfo = result.city;
                    this.userCity = cityinfo.replace("市",""); 
                    this.currCity = cityinfo.replace("市",""); 
                }
            } else {
                console.log('can not find the city for user');
            }
        });
    }

    goToSearchCity(){
      this.navCtrl.push(SearchCityPage);
    }

    goToStatusPage(){
      this.navCtrl.push(ChargingStatusPage);
    }

    getStations(cityCode:any,mobile:any){
        this.myHttp.post('device/findStations',{'cityCode':cityCode,'mobile':mobile}).subscribe(
          data =>{
            if(!data.msg){
              this.stations= data;
              this.generateMarkers();
            }else{
              this.myTips.presentToast(data.msg,3000,"bottom");
            }
              //this.generateMarkers();
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom");
          } 
      )
    }
   checkCharingStatus(){
      this.myHttp.post('service/findProcessOrder',{'mobile' : this.userInfo.mobile}).subscribe(
          data =>{
              if(!data.hasOwnProperty("order_code")){
                  this.isCharging=false;
              }
              
              if(data.hasOwnProperty("msg")){
                  this.isCharging = false;
                  this.myTips.presentToast(data.msg,3000,"bottom")
              }else if(data.hasOwnProperty("order_code")){
                   this.isCharging = true;
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
   }

   checkAppVersion(){

     if(this.plt.is('cordova')){
          AppVersion.getVersionNumber().then((v) => {
              this.myHttp.post('other/getAppVersion',{}).subscribe(
                  data =>{
                      if(data.c2){
                          if(data.c2!=v){
                                 let confirm = this.alertCtrl.create({
                                    title: '程序更新',
                                    message: '应用有更新，点击确认前往更新',
                                    buttons: [
                                        {
                                        text: '取消',
                                          handler: () => {
                                            this.plt.exitApp();
                                          }
                                        },
                                        {
                                        text: '确定',
                                          handler: () => {
                                              window.open("http://www.baidu.com");
                                          }
                                        }
                                    ]
                                  });
                                confirm.present();
                          }
                      }
                  },
                  error =>{
                  } 
              )
          });
    }
     
   }
}
