import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import {OrderCommentPage} from './../order-comment/order-comment';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import {StopResultPage} from './../stop-result/stop-result';
import { MyStoarge } from './../../providers/my-stoarge';
import {ChargingStatusPage} from './../charging-status/charging-status';

/*
  Generated class for the OrderCenter page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-order-center',
  templateUrl: 'order-center.html'
})
export class OrderCenterPage {

  selectedSeg:string='allOrders';

  public userInfo:any;

  public orders=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public myTips: MyTips,
    public myStoarge: MyStoarge,
    public myHttp: MyHttp,
    public alertCtrl : AlertController,) {
        if(navParams.get("index")){
            this.selectedSeg = "pendingComments";
        }
    }

  ionViewDidLoad() {
    this.userInfo = this.myStoarge.getObject("user_info");
    this.getAllOrders();
  }

  ionViewWillEnter(){
      console.log("------------------------"+this.navParams);
  }

  reloadData=()=>{
      return new Promise((resolve, reject) => {
            this.getAllOrders();
            resolve();
     });
  }

  goToOrderComment(event,order){
    event.stopPropagation();
    this.navCtrl.push(OrderCommentPage,{"order":order,"callBack":this.reloadData});
  }

  goToChargingStatus(order){
    if(order.status==1){
        this.navCtrl.push(ChargingStatusPage);
    }
    return;
  }

  goToOrderPay(event,order){
       event.stopPropagation();
       let confirm = this.alertCtrl.create({
            title: '你确定要付款此订单?',
            message: '<span class="pay-money">金额:'+order.charging_money+'&nbsp;&nbsp;(元)</span>',
            buttons: [
                {
                text: '取消',
                handler: () => {
                    console.log('Disagree clicked');
                }
                },
                {
                text: '确定',
                handler: () => {
                    let myPresent= this.myTips.presentLoading("ios-small","正在扣款...");
                     myPresent.present();
                     this.myHttp.post('service/accountPayment',{'orderCode':order.order_code})
                     .subscribe(data=>{
                          myPresent.dismiss();
                          if(data.sucess){
                              this.myTips.presentToast('付款成功',2000,"bottom");
                              this.getAllOrders();
                          }else{
                               let alert = this.alertCtrl.create({
                                  title: '扣款失败',
                                  subTitle: data.msg,
                                  buttons: ['确定']
                                });
                                alert.present();
                          }
                     },
                     error=>{
                         myPresent.dismiss();
                         this.myTips.presentToast('服务器异常！',2000,"bottom");
                     });
                  }
                }
            ]
            });
            confirm.present();

       
  }


  getAllOrders(){
    this.myHttp.post('service/findOrders',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{

          if(data.hasOwnProperty("msg")){
              this.myTips.presentToast(data.msg,2000,"bottom")
          }else{
             this.orders = data;
             this.orders.forEach(function(order){
                  if(order.status==1){
                      order.text = '正在充电';
                  }else if(order.status==2){
                      order.text = '订单完结,未付款';
                  }else if(order.status==3){
                      order.text = '订单完结，已付款';
                  }
             });
          }

      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

}
