import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { LoginPage } from './../login/login';
import { MyTips } from './../../providers/my-tips';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {

  public userName:any;
  public checkCode:any;
  public passWord:any;
  public rePassWord:any;
  public userAlias:any;

  public eye1 : any = "eye";
  public eye2 : any = "eye";
  public passwordType1 : string = "password";
  public passwordType2 : string = "password";
  public checkedCode : any = "获取验证码";

  constructor(public navCtrl: NavController, public navParams: NavParams,
   public myHttp: MyHttp,public alertCtrl: AlertController,public myTips:MyTips) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  TimeDecrease(time){
    
  }
  getCode() {

    if (this.checkedCode == "获取验证码") {
      //这里发送注册账户请求

      this.checkedCode = 60;
      let timer1 = setInterval(() => {
        if (this.checkedCode == 0) {
          this.checkedCode = "获取验证码"
          clearInterval(timer1);
        }else{
          this.checkedCode--;
        }
      }, 1000);

      this.myHttp.post('service/sendSms',{'mobile':this.userName}).subscribe(
        data =>{
            if(data.sucess){
                this.checkedCode = "获取验证码"
                clearInterval(timer1);
            }else{
                let alert = this.alertCtrl.create({
                  title: '',
                  subTitle: '获取验证码失败',
                  buttons: ['确定']
                });
                this.checkedCode = "获取验证码"
                clearInterval(timer1);
                alert.present();
            }
        },
        error => console.log(error)
      );

     

    } else {
      return;
    }

  }
  changeEye1(){
    this.eye1 = !this.eye1 ? "eye" : null;    
    this.passwordType1 = !this.eye1 ? "text" : "password";
  }
  changeEye2(){
    this.eye2 = !this.eye2 ? "eye" : null;    
    this.passwordType2 = !this.eye2 ? "text" : "password";
  }
  forg(){
    console.log("忘记密码！")

    
  }
  ok(){
    if(this.userName&&this.checkCode&&this.rePassWord&&this.checkCode&&this.userAlias){
    this.myHttp.post('service/registerAppUser',{'mobile':this.userName,
    'code' : this.checkCode,'password' : this.passWord,'sex':'','carType' : '','alias':this.userAlias}).subscribe(
        data =>{
            console.log(data);
            if(data.sucess){
                 let alert = this.alertCtrl.create({
                  title: '注册成功',
                  subTitle: '立即登录',
                  buttons: [
                    {text:'确定',
                     handler:()=>{
                        this.navCtrl.push(LoginPage);
                     }}]
                });
                alert.present();
            }else{
                 let alert = this.alertCtrl.create({
                  title: '',
                  subTitle: data.msg,
                  buttons: ['确定']
                });
                alert.present();
            }
        },
        error => console.log(error)
      );
    }else{
        this.myTips.presentToast("输入信息不完整",2000,"bottom");
    }
  }

}
