import { Component } from '@angular/core';
import { NavController, NavParams ,ActionSheetController} from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import {StationDetailPage} from './../station-detail/station-detail';
import { MyGlobals } from './../../providers/my-globals';

/*
  Generated class for the Favorite page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-favorite',
  templateUrl: 'favorite.html'
})
export class FavoritePage {

  public stations=[];
  public userInfo:any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public myHttp: MyHttp,
    public myTips:MyTips,
    public actionSheetCtrl: ActionSheetController,
    public myGlobals:MyGlobals,
    public myStoarge: MyStoarge,) {
        this.userInfo = this.myStoarge.getObject("user_info");
        this.findCollectStations();
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritePage');
  }
  itemSelected(station){
    this.navCtrl.push(StationDetailPage,station); 

  }

  presentActionSheet(event:Event,station:any) {
    event.stopPropagation();
   let actionSheet = this.actionSheetCtrl.create({
     buttons: [
       {
         text: '高德地图',
         handler: () => {
            let from = this.myGlobals.getMyPosition()[0]+","+this.myGlobals.getMyPosition()[1];
            let to = station.longitude+","+station.latitude;
            console.log(from);
              var navUrl = "http://uri.amap.com/navigation?from="+from+",startpoint&to="+to+",endpoint&mode=car&policy=1&src=mypage&coordinate=gaode&callnative=1";
            window.location.href=navUrl;
         }
       },
       {
         text: '取消',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });
   actionSheet.present();
 }

  findCollectStations(){
    this.myHttp.post('device/findCollectStations',{'mobile':this.userInfo.mobile}).subscribe(
          data =>{
            if(data.hasOwnProperty('msg')){
              this.myTips.presentToast('无数据',3000,"bottom")
            }else{
               this.stations = data;
               if(this.stations){
                  this.stations.forEach((station)=>{
                    let lnglat = new AMap.LngLat(this.myGlobals.getMyPosition()[0], this.myGlobals.getMyPosition()[1]);
                    let distant = Math.floor(lnglat.distance([station.longitude, station.latitude])); 
                    station.distant = distant;
                });
               }
            }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          }
      )
  }

  removeItem(event:Event,station,sIndex){
    event.stopPropagation();
    this.myHttp.post('other/deleteCollect',{'mobile':this.userInfo.mobile,
      'stationIds' : station.id}).subscribe(
          data =>{
              if(data.sucess === true){
                this.stations.splice(sIndex);
                this.myTips.presentToast('操作成功',2000,"bottom")
              }else{
                this.myTips.presentToast(data.msg,2000,"bottom")
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",2000,"bottom")
          } 
    );
  }

}
