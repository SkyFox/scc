import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {RechargePage} from './../recharge/recharge';
import {TransactionDetailsPage} from './../transaction-details/transaction-details';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';

/*
  Generated class for the MyMoney page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-my-money',
  templateUrl: 'my-money.html'
})
export class MyMoneyPage {

  public userInfo={
    mobile : '',
    myMoney : ''
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myStoarge: MyStoarge,
  public myHttp: MyHttp,
  public myTips: MyTips,) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyMoneyPage');
    this.getAccountMoney();
  }

  reloadMoney=()=>{
     return new Promise((resolve, reject) => {
        this.getAccountMoney();
        resolve();
     })
  }

  goToRecharge(){
      this.navCtrl.push(RechargePage,{"callBack":this.reloadMoney});
  }
  goToTransaction(){
      this.navCtrl.push(TransactionDetailsPage);
  }
  getAccountMoney(){
    this.userInfo = this.myStoarge.getObject("user_info");
    this.myHttp.post('service/findAccountMoney',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{
            this.userInfo.myMoney = data.account_money;
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }
  

}
