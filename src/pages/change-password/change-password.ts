import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';

/*
  Generated class for the ChangePassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html'
})
export class ChangePasswordPage {

  public userInfo:any;
  public newPwd:any;
  public oldPwd:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
    public myTips:MyTips,
   public myStoarge: MyStoarge,
   public myHttp: MyHttp,
   public alertCtrl : AlertController,) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
    this.userInfo = this.myStoarge.getObject("user_info");
  }
  changePassword(){
      
      if(!this.newPwd||!this.oldPwd){
        this.myTips.presentToast('密码信息不完整',3000,"bottom");
        return;
      }

      let confirm = this.alertCtrl.create({
            title: '修改密码',
            message: '你确定要修改密码?',
            buttons: [
                {
                text: '取消',
                handler: () => {
                    console.log('Disagree clicked');
                }
                },
                {
                text: '确定',
                handler: () => {
                    this.myHttp.post('service/modifyPassword',{'mobile':this.userInfo.mobile,
                      'newPwd':this.newPwd ,'oldPwd': this.oldPwd}).subscribe(
                              data =>{
                                if(data.sucess == true){
                                   this.myTips.presentToast('修改密码成功',3000,"bottom")
                                }else{
                                  this.myTips.presentToast(data.msg,3000,"bottom")
                                }
                              },
                              error =>{
                                  console.log(error);
                                  this.myTips.presentToast("服务器异常！",3000,"bottom")
                              } 
                          )
                }
                }
            ]
            });
            confirm.present();
  }

}
