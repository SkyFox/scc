import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';

/*
  Generated class for the Feedback page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html'
})
export class FeedbackPage {

  public userInfo:any;

  public content:string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myHttp: MyHttp,public myTips:MyTips,
  public myStoarge: MyStoarge,public alertCtrl : AlertController,) {
  }

  ionViewDidLoad() {
    this.userInfo = this.myStoarge.getObject("user_info");
    console.log('ionViewDidLoad FeedbackPage');
  }

  submit(){
       let myPresent= this.myTips.presentLoading("ios-small","提交中...");
        myPresent.present();
       this.myHttp.post('other/inputOpinion',{"mobile":this.userInfo.mobile,'content':this.content}).subscribe(
        data =>{
          if(data.sucess === true){
              myPresent.dismiss();
          let confirm = this.alertCtrl.create({
            title: '',
            message: '意见反馈成功',
            buttons: [
                {
                text: '确定',
                handler: () => {
                         this.navCtrl.pop();
                  }
                }
            ]
            });
            confirm.present();

          }else if(data.sucess === false){
              this.myTips.presentToast(data.msg,3000,"bottom")
          }
        },
        error =>{
            console.log(error);
            this.myTips.presentToast("服务器异常！",3000,"bottom")
        } 
    )
  }
}
