import { Component,ViewChild,HostBinding ,ChangeDetectorRef} from '@angular/core';
import { NavController, NavParams ,Platform,Content,Events} from 'ionic-angular';
import { Http,Response,Headers, RequestOptions } from '@angular/http';
import { MyTips } from './../../providers/my-tips';
import { MyHttp } from './../../providers/my-http';

/*
  Generated class for the SearchCity page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-search-city',
  templateUrl: 'search-city.html'
})
export class SearchCityPage {
  @ViewChild(Content) content: Content;
  public cities:any;
  public myPresent:any;
  public chars:any="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  public indexs:any[]=[];
  public hIndex:any;
  public hideMiddle:boolean=true;
  public timeoutId:any;
  public hintTop:any;
  public hint:any;
  public searchCityList:any[]=[];
  public tempCityList:any[]=[];
  

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public http: Http, public myTips: MyTips,public myHttp: MyHttp,
  public plt:Platform,
  public changeDetectorRef: ChangeDetectorRef,
  public event :Events) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchCityPage1');
    this.myPresent= this.myTips.presentLoading("ios-small","城市列表加载中...");
    this.hIndex =(this.plt.height()-130)/26+"px"; 
    this.hintTop=(this.plt.height()-50)/2+"px";
    this.myPresent.present();
     for(var i=0;i<this.chars.length;i++){
        this.indexs.push(this.chars.charAt(i));//获取字母数组 
    }
    
  }

  ionViewDidEnter(){
    console.log('ionViewDidLoad SearchCityPage2');
    this.getCities();
    this.myPresent.dismiss();
  }


  getCities(){
        /*this.http.get("service/findCity").subscribe(res => {
              // we've got back the raw data, now generate the core schedule data
              // and save the data for later reference
              this.cities = res.json().dataList;
              this.searchCityList = res.json().dataList;
          });*/
          this.myHttp.post('service/findCity',{}).subscribe(
            data =>{
              if(data.sucess){
                  this.cities = JSON.parse(data.msg);
                  this.tempCityList = Object.assign([], this.cities);
                  console.log(JSON.parse(data.msg));
              }else{
                this.myTips.presentToast(data.msg,3000,"bottom")
              }
          },
          error =>{
              console.log(error);
              this.myTips.presentToast("服务器异常！",3000,"bottom")
          } 
      )
  }

  showHint(letter:any){
     this.hideMiddle=false;
     this.hint=letter;
     clearTimeout(this.timeoutId);
     this.timeoutId=setTimeout(() => {
          this.hideMiddle=true;
        }, 2000);
  }

  getItems(event){
       this.tempCityList = Object.assign([], this.cities);
       let  tmpCities =  this.cities.filter((cityObj)=>{
          let tmpCity= cityObj.datas.filter((city)=>{
              return city.zoneCity.indexOf(event.target.value) > -1;
          });

          if(tmpCity.length>0){
            cityObj.datas.splice(0,cityObj.datas.length);
            cityObj.datas =  tmpCity;
            return cityObj
          }
     });
    this.tempCityList.splice(0,this.cities.length);
     this.tempCityList =  tmpCities;
  }

  directyToCity(city){
     let item = {
        zoneCity : city,
        zoneCityId : '027'
     };
     this.navCtrl.pop();
     this.event.publish('city:selected', item);

  }

  itemSelected(city:any){
       this.navCtrl.pop();
       this.event.publish('city:selected', city);
       
  }

}
