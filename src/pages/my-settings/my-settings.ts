import { Component } from '@angular/core';
import { NavController, NavParams,Platform } from 'ionic-angular';
import {PersonInfoPage} from './../person-info/person-info';
import { MyStoarge } from './../../providers/my-stoarge';
import {AboutUsPage} from './../about-us/about-us';
import {ChangePasswordPage} from './../change-password/change-password';

/*
  Generated class for the MySettings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-my-settings',
  templateUrl: 'my-settings.html'
})
export class MySettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public platform:Platform,
  public myStoarge: MyStoarge) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad MySettingsPage');
  }

  exitApp(){
    let userInfo = this.myStoarge.setObject("user_info",null);
    this.platform.exitApp();
  }

  goToPersonalInfo(){
    this.navCtrl.push(PersonInfoPage);
  }
  aboutUs(){
      this.navCtrl.push(AboutUsPage);
  }

  changePassword(){
      this.navCtrl.push(ChangePasswordPage);
  }

}
