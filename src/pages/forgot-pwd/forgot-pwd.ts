import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';

/*
  Generated class for the ForgotPwd page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-forgot-pwd',
  templateUrl: 'forgot-pwd.html'
})
export class ForgotPwdPage {

  public checkCode:any;
  public passWord:any;
  public rePassWord:any;
  public mobile:any;

  public eye1 : any = "eye";
  public eye2 : any = "eye";
  public passwordType1 : string = "password";
  public passwordType2 : string = "password";
  public checkedCode : any = "获取验证码";

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public tipCtrl:MyTips,public httpCtrl:MyHttp,public myTips:MyTips) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPwdPage');
  }

  changeEye1(){
    this.eye1 = !this.eye1 ? "eye" : null;    
    this.passwordType1 = !this.eye1 ? "text" : "password";
  }
  changeEye2(){
    this.eye2 = !this.eye2 ? "eye" : null;    
    this.passwordType2 = !this.eye2 ? "text" : "password";
  }
  getCode(){
      if (this.checkedCode == "获取验证码") {
        
        this.checkedCode = 60;
        let timer1 = setInterval(() => {
          if (this.checkedCode == 0) {
            this.checkedCode = "获取验证码"
            clearInterval(timer1);
          }else{
            this.checkedCode--;
          }
        }, 1000);

        this.httpCtrl.post('service/sendSms',{'mobile':this.mobile}).subscribe(
          data =>{
              if(data.sucess){

              }else{
                 this.checkedCode = "获取验证码"
                 clearInterval(timer1);
                this.tipCtrl.presentToast(data.msg,2000,"bottom")
              }
          },
          error => {
            this.checkedCode = "获取验证码"
            clearInterval(timer1);
            this.tipCtrl.presentToast("服务器异常！",2000,"bottom")
          }
        );
    } else {
      return;
    }
  }
  forg(){
    console.log("忘记密码！")
  }
  ok(){
    if(this.mobile&&this.passWord&&this.rePassWord&&this.checkCode){
     this.httpCtrl.post('service/forgetPassword',{'mobile':this.mobile,
     'code' : this.checkCode,'password' : this.passWord}).subscribe(
        data =>{
            if(data.sucess){
                 this.tipCtrl.presentToast("修改密码成功",2000,"bottom");
            }else{
                this.tipCtrl.presentToast(data.msg,2000,"bottom");
            }
        },
        error => {
          this.tipCtrl.presentToast("服务器异常！",2000,"bottom");
        }
      );
    }else{
       this.myTips.presentToast("输入信息不完整",2000,"bottom");
    }
  }

}
