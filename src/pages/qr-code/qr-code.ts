import { Component } from '@angular/core';
import { NavController, NavParams ,ModalController,AlertController,App} from 'ionic-angular';
import { BarcodeScanner } from 'ionic-native';
import {InputValidnoPage} from './../input-validno/input-validno';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import {VaildSuccessPage} from './../vaild-success/vaild-success';
import { MyStoarge } from './../../providers/my-stoarge';
import { PayNotesPage } from './../pay-notes/pay-notes';
import {LoginPage} from './../login/login';


/*
  Generated class for the QRCode page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html'
})
export class QRCodePage {

  public portCode:any;
  public userInfo:any;
  public serialNumber:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myHttp: MyHttp,
  public tipCtrl:MyTips,
  public modalCtrl : ModalController,
  public myStoarge: MyStoarge,
  public alertCtrl:AlertController,
  public app:App) {}

  ionViewDidLoad() {
    this.userInfo = this.myStoarge.getObject("user_info");
    console.log('ionViewDidLoad QRCodePage');
  }

  scanNo(){
    if(this.userInfo){
        BarcodeScanner.scan().then((barcodeData) => {
        this.myHttp.post('device/verifyDevice',
              {"portCode":barcodeData.text,"loginId":this.userInfo.mobile}).subscribe(
                data =>{
                  this.portCode = barcodeData.text;
                  if(data.sucess==true){
                    this.serialNumber = data.msg;
                    let myPresent= this.tipCtrl.presentLoading("ios-small","认证查询中...");
                    myPresent.present();
                    this.queryVerifyResult(1,myPresent);
                  }else{
                    let tip=this.tipCtrl.showAlert('',data.msg,'确定');
                  }
                },
                error => console.log(error)
              )
        }, (err) => {
            // An error occurred
        });
    }else{
        this.showTips();
    }
  }

  showTips(){
      let alert = this.alertCtrl.create({
          title: '请先登录',
        });
        alert.addButton({ text: '取消', handler:()=>{
          return;
        }  });
        alert.addButton({ text: '登录', handler:()=>{
            this.app.getRootNav().setRoot(LoginPage);
        }});

        alert.present();
  }

  queryVerifyResult(status:any,myPresent:any){
      if(status==1){
          this.myHttp.post('device/verifyDeviceResult',
           {"serialNumber":this.serialNumber}).subscribe(
            data =>{
              if(data.flag==1){
                    myPresent.dismiss();
                    let modal = this.modalCtrl.create(VaildSuccessPage,data);
                        modal.present();
                    modal.onDidDismiss(()=>{
                        this.navCtrl.pop();
                    });
                    this.getChargingResult();
                    this.userInfo.portCode = this.portCode;
                    this.myStoarge.setObject("user_info" ,this.userInfo);
              }else if(data.flag==2){
                    this.queryVerifyResult(1,myPresent);
              }else if(data.flag==0){
                  myPresent.dismiss();
                  if(data.msg==1){
                    this.tipCtrl.presentToast('此设备尚未插枪',2000,"bottom");
                  }else if(data.msg==2){
                    this.tipCtrl.presentToast('设备检测失败',2000,"bottom");
                  }else if(data.msg==3){
                    this.tipCtrl.presentToast('此设备已被占用',2000,"bottom");
                  }
              }else{
                  myPresent.dismiss();
                  let tip=this.tipCtrl.showAlert('',data.msg,'确定');
              }
            },
            error => {
                myPresent.dismiss();
                this.tipCtrl.presentToast('服务器异常',2000,"bottom")
            }
          )
      }
  }

  inputScNo(){
    if(this.userInfo){
      this.navCtrl.push(InputValidnoPage);
    }else{
      this.showTips();
    }
  }

   getChargingResult(){
    this.myHttp.post('device/startCharingResult',{'portCode':this.portCode,"loginId":this.userInfo.mobile}).subscribe(
        data =>{
          if(data.flag == 2){
              this.getChargingResult();
          }
        },
        error =>{
            console.log(error);
        } 
    )
  }

  goToUsageNotes(){
      this.navCtrl.push(PayNotesPage);
  }

}
