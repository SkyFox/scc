import { Component,ChangeDetectorRef} from '@angular/core';
import { NavController, NavParams ,AlertController ,ModalController} from 'ionic-angular';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import {StopResultPage} from './../stop-result/stop-result';
import { MyStoarge } from './../../providers/my-stoarge';


/*
  Generated class for the ChargingStatus page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-charging-status',
  templateUrl: 'charging-status.html'
})
export class ChargingStatusPage {

  public userInfo:any;

  public chargingPercent='0%';

  public processResult={
    soc:'',
    alreadyChargingTime:'',
    alreadyChargingAmount:'',
    estimatedAmount:'',
    voltage:'',
    electricCurrent :'',
    batteryMaxVoltage : '',
    batteryMinVoltage:'',
    batteryMaxTemperature:'',
    batteryMinTemperature : ''
  };

  public timer:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myHttp: MyHttp,public myTips:MyTips,
   public changeDetectorRef: ChangeDetectorRef,
   public alertCtrl : AlertController,
   public modalCtrl : ModalController,
   public myStoarge: MyStoarge,) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChargingStatusPage');
  }
  ionViewDidEnter(){
      this.userInfo = this.myStoarge.getObject("user_info");
      this.getChargingResult();
  }

  ionViewDidLeave(){
     clearInterval(this.timer);
  }

  getChargingResult(){
    let myPresent= this.myTips.presentLoading("ios-small","充电信息获取中...");
    myPresent.present();
    this.myHttp.post('device/startCharingResult',{'portCode':this.userInfo.portCode,"loginId":this.userInfo.mobile}).subscribe(
        data =>{
          if(data.flag == 1){
              this.getQueryProcessResult(1,myPresent);
              this.timer = setInterval(()=>{
                    this.getQueryProcessResult(1,myPresent);
              },10000);
          }else if(data.flag==2){
              this.getChargingResult();
          }else{
              this.myTips.presentToast(data.msg,3000,"bottom");
          }
        },
        error =>{
            console.log(error);
            this.myTips.presentToast("服务器异常！",3000,"bottom");
        } 
    )
  }

  getQueryProcessResult(status:any,myPresent:any){
      if(status==1){
          this.myHttp.post('device/charingProcessResult',{'portCode':this.userInfo.portCode}).subscribe(
              data =>{
                if(data.flag == 1){
                    this.processResult = JSON.parse(data.msg);
                    this.chargingPercent = this.processResult.soc+"%"; 
                    myPresent.dismiss();
                }else if(data.flag == 2){
                    this.getQueryProcessResult(1,myPresent);
                }else{
                  this.myTips.presentToast(data.msg,3000,"bottom");
                  myPresent.dismiss();
                }
              },
              error =>{
                  console.log(error);
                  this.myTips.presentToast("服务器异常！",3000,"bottom");
                  myPresent.dismiss();
              } 
          )
      }
  }

  stopCharging(){
        let confirm = this.alertCtrl.create({
            title: '结束充电?',
            message: '你确定要结束此次充电?',
            buttons: [
                {
                text: '取消',
                handler: () => {
                    console.log('Disagree clicked');
                }
                },
                {
                text: '确定',
                handler: () => {
                    let myPresent= this.myTips.presentLoading("ios-small","正在停电中...");
                    myPresent.present();
                    this.stopAction(myPresent);
                }
                }
            ]
            });
            confirm.present();
  }

  stopAction(present:any){
       this.myHttp.post('device/stopCharging',{'portCode':this.userInfo.portCode,
       'loginId':this.userInfo.mobile}).subscribe(
              data =>{
                if(data.sucess == true){
                    this.queryStopResult(1,present);
                }else{
                  this.myTips.presentToast(data.msg,3000,"bottom")
                }
              },
              error =>{
                  console.log(error);
                  this.myTips.presentToast("服务器异常！",3000,"bottom")
              } 
          )
  }

  queryStopResult(status:any,present:any){
      if(status==1){
            this.myHttp.post('device/stopChargingResult',{'portCode':this.userInfo.portCode,
                "loginId":this.userInfo.mobile}).subscribe(
                        data =>{
                            if(data.flag == 1){
                                present.dismiss();
                                let modal = this.modalCtrl.create(StopResultPage,JSON.parse(data.msg));
                                 modal.present();
                            }else if(data.flag==2){
                                this.queryStopResult(1,present);
                                this.myTips.presentToast(data.msg,3000,"bottom")
                            }else{
                                present.dismiss();
                                this.myTips.presentToast(data.msg,3000,"bottom")
                            }
                        },
                        error =>{
                            present.dismiss();
                            console.log(error);
                            this.myTips.presentToast("服务器异常！",3000,"bottom")
                        } 
                    )
            }
  }

}
