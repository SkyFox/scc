
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { MyTips } from './../../providers/my-tips';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';

import { RegisterPage } from './../register/register';

import { TabsPage } from './../tabs/tabs';

import { ForgotPwdPage } from './../forgot-pwd/forgot-pwd'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public User ={
    mobile:"",
    password:"",
    carType : "",
    alias : ""
  }
  public eye : any = "eye";
  public passwordType : string = "password";
  public copyright: any;

  constructor(public navCtrl: NavController,
                      public navParams: NavParams,
                      public myTips: MyTips,
                      public myStoarge: MyStoarge,
                      public myHttp: MyHttp,) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  inputFocus(){
    this.copyright = "copyright";
  }
  inputBlur(){
    this.copyright = null;
  }
  changeEye(){
    this.eye = !this.eye ? "eye" : null;    
    this.passwordType = !this.eye ? "text" : "password";
  }
  login(){

    if(this.User.mobile&&this.User.password){
      let myPresent= this.myTips.presentLoading("ios-small","登录中...");
      myPresent.present();
      this.myHttp.post('service/login',this.User).subscribe(
        data =>{
          if(data.sucess === true){
            let tempUser = JSON.parse(data.msg);
            this.User.carType = tempUser.carType;
            this.User.alias = tempUser.alias;
            this.myStoarge.setObject("user_info",this.User);
            myPresent.dismiss();
            this.navCtrl.setRoot(TabsPage);
          }else if(data.sucess === false){
            myPresent.dismiss();
            this.myTips.presentToast("用户名或密码错误！",2000,"bottom");
          }
        },
        error =>{
            myPresent.dismiss();
            this.myTips.presentToast("服务器异常！",2000,"bottom");
        } 
      )
      }else{
          this.myTips.presentToast("请输入用户名或密码",2000,"bottom");
      }
  }
  resg(){
    this.navCtrl.push(RegisterPage);
  }
  forg(){
   this.navCtrl.push(ForgotPwdPage);
  }

}
