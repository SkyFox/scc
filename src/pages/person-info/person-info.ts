import { Component } from '@angular/core';
import { NavController, NavParams,ModalController } from 'ionic-angular';
import { MyStoarge } from './../../providers/my-stoarge';
import { MyHttp } from './../../providers/my-http';
import { MyTips } from './../../providers/my-tips';
import {UpdateUserPage} from './../update-user/update-user';

/*
  Generated class for the PersonInfo page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-person-info',
  templateUrl: 'person-info.html'
})
export class PersonInfoPage {

  public userInfo:any;

  public personInfo={
    loginId:"",
    carType:"",
    alias:""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public myStoarge: MyStoarge,
   public myTips: MyTips,
    public myHttp: MyHttp,
     public modalCtrl : ModalController,
  ) {
      this.userInfo = this.myStoarge.getObject("user_info");
      this.getPersonInfo();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PersonInfoPage');
  }

  ionViewDidEnter(){
      this.getPersonInfo();
  }

  modifyUser(){
      let modal = this.modalCtrl.create(UpdateUserPage);
      modal.present();
  }


  getPersonInfo(){
      this.myHttp.post('service/getAppUserInfo',{'mobile':this.userInfo.mobile}).subscribe(
      data =>{
          if(data.sucess===true){
              this.personInfo = JSON.parse(data.msg);
          }else{
            this.myTips.presentToast(data.msg,2000,"bottom")
          }
      },
      error =>{
          console.log(error);
          this.myTips.presentToast("服务器异常！",2000,"bottom")
      } 
    )
  }

}
